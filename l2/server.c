#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>

#include <unistd.h>
#include <arpa/inet.h>
#include <netdb.h>

#define MAXBUFLEN 1280
struct packet* decodePacket(char* recvPack, int numBytes);
void *get_in_addr(struct sockaddr *sa);

struct packet {
	unsigned int total_frag;
	unsigned int frag_no;
	unsigned int size;
	char *filename;
	char filedata[1000];
};

struct fileinuse { // Linked list of files, in case we have concurrent clients
	char* filename;
	FILE* fileptr;
	int frag_no;
  	struct fileinuse* next;
};

struct fileinuse* filelist = NULL;


// Creates a new file and populates it according to packet information.
// Retuns a pointer to the struct, which is the new head of filelist.
struct fileinuse* createfilestruct (struct fileinuse* filelist, struct packet* packet) {
  if (packet == NULL) { //Bad input
		printf("Bad input to createfilestruct\n");
		return NULL;
	}
  //create file struct
	struct fileinuse* newfile = malloc (sizeof(struct fileinuse));
	//populate file struct
	newfile->filename = malloc (sizeof(char) * (strlen(packet->filename) + 1));
	strncpy(newfile->filename, packet->filename, strlen(packet->filename) + 1);
	newfile->fileptr = fopen(packet->filename,"ab"); //append binary mode
	newfile->frag_no = 1;
	newfile->next = filelist;
	return newfile;
}

// Closes a file and deletes its struct.
// Returns pointer to head of filelist (which may have
// been modified) success, or NULL if file not found.
struct fileinuse* deletefilestruct (struct fileinuse* filelist, char* filename) {
	if (filelist == NULL || filename == NULL) { //Bad input
		printf("Bad input to deletefilestruct\n");
		return NULL;
	}
	if (!strcmp(filelist->filename, filename)) { //Deleting head
		struct fileinuse* newhead = filelist->next;
		free(filelist->filename);
		fclose(filelist->fileptr);
		free(filelist);
		return newhead;
	}
	struct fileinuse *prev = filelist, *curr = filelist->next;
	while (curr != NULL && strcmp(curr->filename, filename)) { // Look for file
		curr = curr->next;
		prev = prev->next;
	}
	if (curr == NULL) {
		printf("Failed to find file in deletefilestruct\n");
		return NULL;
	}
	prev->next = curr->next;
	free(curr->filename);
	fclose(curr->fileptr);
	free(curr->fileptr);
	free(curr);
	return filelist;
}

// Writes packet to corresponding file. Creates file if first packet
// processed and closes file if last packet processed.
// Returns pointer to head of filelist, which may have been modified.
struct fileinuse* writefile (struct fileinuse* filelist, struct packet* packet) {
	struct fileinuse* list = filelist;
	while (list != NULL && strcmp(list->filename, packet->filename)) { // Look for file
		list = list->next;
	}
	if (list == NULL) { // File not found
		filelist = createfilestruct(filelist, packet);
		list = filelist;
	}
	if (list->frag_no > packet->frag_no) {
		printf("Received duplicate packet.\n"); //ACK was dropped, or just a slow network
		return filelist;
	}
	else if (list->frag_no < packet->frag_no) {
		printf("Received out of order packet.\n");
		// Unsure how to deal with this. Only happens with malicious middleman
		// Could send NACK and force client to start from the beginning again
	}
	int i = 0;
	for(i; i < packet->size; i++) { // Write packet data to file
		fputc(packet->filedata[i], list->fileptr);
	}
	list->frag_no++;
	if (packet->frag_no == packet->total_frag) {
		filelist = deletefilestruct (filelist, packet->filename);
	}
	return filelist;
}

int main (int argc, char* argv[]) {
	if (argc != 2) {
		printf("Proper Usage:\n");
		printf("server <listenport>\n");
		return -1;
	}


	 int sockfd,rv, numbytes;
	 socklen_t addressLength;
	 char buf[MAXBUFLEN];
	 char s[INET6_ADDRSTRLEN];
	 struct addrinfo sockGen, *servinfo, *sockList;
	 struct sockaddr_storage recvAddr;

	
	 addressLength = sizeof(recvAddr);
	
	 memset(&sockGen, 0, sizeof(struct addrinfo));
	 sockGen.ai_family = AF_UNSPEC;
	 sockGen.ai_socktype = SOCK_DGRAM; 
	 sockGen.ai_flags = AI_PASSIVE;

	 if ((rv = getaddrinfo(NULL, argv[1], &sockGen, &servinfo)) != 0) {
	 	printf("Server: get addy error = %s\n", gai_strerror(rv));
	 	return 1;
	 }

	 //Attemp bind
	 for(sockList = servinfo; sockList != NULL; sockList = sockList->ai_next) {
		 if ((sockfd = socket(sockList->ai_family, sockList->ai_socktype, sockList->ai_protocol)) == -1) continue;
		 if (bind(sockfd, sockList->ai_addr, sockList->ai_addrlen) == -1) {
			 close(sockfd);
			 continue;
		 }
		 break;
	 }
	 if (sockList == NULL) {
		 printf("Server: failed to bind socket\n");
		 return 2;
	 }
	 freeaddrinfo(servinfo);

		
		//Recieve data
	 while (1){

		 if ((numbytes = recvfrom(sockfd, buf, MAXBUFLEN , 0, (struct sockaddr*)&recvAddr, &addressLength)) == -1) {
		 	printf("Server: failed to receive\n");
		 } 
		 
			//fills the packet object with the buffer info
		 struct packet *recvPacket = decodePacket(buf, numbytes);
		 //printf("Server: \n[STARTPACKET]\n%s\n[ENDPACKET]\n", buf);

		 filelist = writefile(filelist, recvPacket);	

		 printf("Server: Received frag #%d of %d from file %s\n", recvPacket->frag_no, recvPacket->total_frag, recvPacket->filename);
		 memset(buf, 0, numbytes);
				 
		 char ACK[4];
		 strcpy(ACK, "YES");
		 //printf("ACKTho: %s\n", ACK);
		 //DO An ACK
		 if (sendto(sockfd, ACK, sizeof(ACK), 0, ((struct sockaddr *)&recvAddr), addressLength) == -1) {
			printf("Server: ACK sending Error\n");
			return -1;
		 	}
		 }

}

void *get_in_addr(struct sockaddr *sa) {
	if (sa->sa_family == AF_INET) return &(((struct sockaddr_in*)sa)->sin_addr);
	return &(((struct sockaddr_in6*)sa)->sin6_addr);
}


struct packet* decodePacket(char* recvPack, int numBytes) {

		struct packet *newPack;
		newPack = malloc(sizeof(struct packet));
		newPack->filename = malloc(256); 
		
		int irecv = 0;
		char c[256];

		int i = 0;
		for(i; recvPack[i+irecv] != ':'; i++) c[i] = recvPack[i+irecv];
		irecv += i + 1;
		c[i+1] = '\0';
		newPack->total_frag = atoi(c);
		memset(c, 0, sizeof(c));

		i = 0;
		for(i; recvPack[i+irecv] != ':'; i++) c[i] = recvPack[i+irecv];
		irecv += i + 1;
		c[i+1] = '\0';
		newPack->frag_no = atoi(c);
		memset(c, 0, sizeof(c));

		i = 0;
		for(i; recvPack[i+irecv] != ':'; i++) c[i] = recvPack[i+irecv];
		irecv += i + 1;
		c[i+1] = '\0';
		newPack->size = atoi(c);
		memset(c, 0, sizeof(c));
		
		i = 0;
		for(i; recvPack[i+irecv] != ':'; i++) c[i] = recvPack[i+irecv];
		irecv += i + 1;
		c[i+1] = '\0';
		strcpy(newPack->filename, c);
		memset(c, 0, sizeof(c));
		
		i = 0;		
		for(i; i + irecv < numBytes; i++) newPack->filedata[i] = recvPack[i+irecv];
		return newPack;
}

// Notes:
// we don't have to deal with bit corruption
// this is due during the demo session
// 
