#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <time.h>

#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>

#define PACKET_SIZE 1000
#define MAXBUFLEN 1280

//Code similar to textbook

struct packet {
	unsigned int total_frag;
	unsigned int frag_no;
	unsigned int size;
	char* filename;
	char filedata[1000];
} packet;

long mSecTDiff(struct timeval start, struct timeval end);
char* encodePacket(struct packet* packet, int *iData);
int main (int argc, char* argv[]) {
	//parse server address
	//parse server port number
	//parse client listen port
	//parse file name
	if (argc != 5) {
		printf("Proper Usage:\n");
		printf("deliver <sendaddy> <sendport> <recieveport> <filename>\n");
		return -1;
	}

	//open file
	FILE* fileptr = fopen(argv[4],"rb");
	if (fileptr == NULL) {
		printf("File does not exist.\n");
		return -1;
	} else printf("File opened.\n");
	
	//find size of file
	fseek (fileptr, 0, SEEK_END);
	int filesize = ftell (fileptr);
	fseek (fileptr, 0, SEEK_SET);
	
	//create packet
	struct packet *packet = malloc (sizeof(struct packet));

	//set up other packet variables
	int frag_no = 1;
	packet->total_frag = (filesize / PACKET_SIZE);
	if (filesize % PACKET_SIZE != 0)
		packet->total_frag++;
		
	packet->size = PACKET_SIZE; //If the packet is smaller, this will be overwritten in the loop
	packet->filename = malloc(sizeof(char) * (strlen(argv[4]) + 1));
	strncpy(packet->filename, argv[4], strlen(argv[4]) + 1);
	
	int sockfd;
	struct addrinfo sockgen, *servinfo, *socketlist;
	
	memset(&sockgen, 0, sizeof(sockgen));
	sockgen.ai_family = AF_UNSPEC;
	sockgen.ai_socktype = SOCK_DGRAM;
	int error = getaddrinfo(argv[1], argv[2], &sockgen, &servinfo);
	if (error != 0)
		printf("%s\n", gai_strerror(error));

	//Attempt to bind to socket
	for (socketlist = servinfo; socketlist != NULL; socketlist = socketlist->ai_next) {
		if ((sockfd = socket(socketlist->ai_family, socketlist->ai_socktype, socketlist->ai_protocol)) != -1) {			
			break;
		}
	}
	if (socketlist == NULL) {
		printf("Failed to bind to socket\n");
		return 1;
	}
	
	//Populate and send packets
	while (frag_no <= packet->total_frag) {
		//Populate packet
		packet->frag_no = frag_no;
		if (frag_no == packet->total_frag)
			packet->size = filesize - PACKET_SIZE * (packet->total_frag - 1);
		
		int i = 0;
		for (i; i < packet->size; i++)
			packet->filedata[i] = fgetc(fileptr);
	
		//Put in the0:0:0:0:0 format
		int *iData = malloc(sizeof(int));
		char *data = encodePacket(packet, iData);

	 	int acknowledged = 0;
		while (acknowledged == 0) {
			
			if (sendto(sockfd, data, *iData, 0, socketlist->ai_addr, socketlist->ai_addrlen) == -1) {
				printf("Sending Error\n");
				return -1;
			}
			printf("Sent packet #%d of %d\n", packet->frag_no, packet->total_frag);


			struct timeval tv;
			//tv.tv_sec = 0;  /* 1 Secs Timeout */
			tv.tv_sec = 1;  /* 1 Secs Timeout */
			//tv.tv_usec = 50000;  // Not init'ing this can cause strange errors
			tv.tv_usec = 0;  // Not init'ing this can cause strange errors
			setsockopt(sockfd, SOL_SOCKET, SO_RCVTIMEO, (char*)&tv,sizeof(struct timeval));
			
			int numbytes;
			char buf[MAXBUFLEN];
			struct sockaddr_storage their_addr;
			socklen_t addr_len = sizeof(their_addr);
			if ((numbytes = recvfrom(sockfd, buf, MAXBUFLEN , 0, (struct sockaddr*)&their_addr, &addr_len)) == -1) {
	 			printf("Deliverer: failed to receive\n");
    	}
    	if (strcmp(buf, "YES") == 0) {
    		acknowledged = 1;
    		printf("ACK received\n");
    	}
		}
		printf("Done sending packet #%d\n",frag_no);
		frag_no++;
  	}
	
	freeaddrinfo(servinfo);
	printf("%s", "\0");
}

long mSecTDiff(struct timeval start, struct timeval end){
	long delay;
	delay = (end.tv_sec - start.tv_sec) * 1000;
	delay += ((end.tv_usec - start.tv_usec + 500) / 1000);
	return delay;

}

char* encodePacket(struct packet* packet, int* returniData) {
	char *data = (char*) malloc(1280);
	int speciData;
	int i = 0;
	char c[32];
	int iData = 0;

	speciData = sprintf(c, "%d", packet->total_frag);
	for (i; i < speciData; i++) data[i+iData] = c[i];
	data[iData+i] = ':';
	iData += i + 1;

	i = 0;
	speciData = sprintf(c, "%d", packet->frag_no);
	for (i; i < speciData; i++) data[i+iData] = c[i];
	data[iData+i] = ':';
	iData += i + 1;

	i = 0;
	speciData = sprintf(c, "%d", packet->size);
	for (i; i < speciData; i++) data[i+iData] = c[i];
	data[iData+i] = ':';
	iData += i + 1;

	i = 0;
	for (i; packet->filename[i] != '\0'; i++) data[i+iData] = packet->filename[i];
	data[iData+i] = ':';
	iData += i + 1;

	i = 0;
	for (i; i < packet->size; i++) data[i+iData] = packet->filedata[i];
	iData += i;
 
	*returniData = iData;
	return data;
}

// Notes:
// we don't have to deal with bit corruption
// 
